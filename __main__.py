
from CompressionManager import CompressionManager
from sys import argv


def main(config_path):
	compression_manager = CompressionManager(config_path)
	compression_manager.run()


if __name__ == '__main__':
	main(argv[0])
