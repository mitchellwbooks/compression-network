import subprocess as sub
from logger import Logger
from videoFile import VideoFile


class CompressionWorker:
    videoFile = VideoFile

    def __init__(self, video_file: VideoFile ):
        self.videoFile = video_file
        input_file_path = '{}/{}'.format(self.videoFile.fileDirectory, self.videoFile.fileName)
        output_file_path = '/scratch/{}'.format(self.videoFile.outputFileName)

        self.command = 'ffmpeg -i {INPUT_FILE} ' \
                       '-threads {CPU_COUNT} ' \
                       '-x265-params pools={CPU_COUNT} ' \
                       '-map 0 -c copy -c:v libx265 -preset slow -crf 23 ' \
                       '-max_muxing_queue_size 1024 {OUTPUT_FILE_PATH}'.format(
            INPUT_FILE = input_file_path, CPU_COUNT = self.videoFile.threadCount, OUTPUT_FILE_PATH = output_file_path)
        print('Worker Created')

    def compressFile(self):
        try:
            print('Worker Started')
            completed = sub.run(self.command, shell = True, check = True)
            Logger('completed task. Completed object: {}'.format(str(completed)))
            completed_move = sub.run('mv /scratch/{} {}'.format(self.videoFile.outputFileName, self.videoFile.outputDirectory + '/' + self.videoFile.outputFileName), shell=True, check = True)
            print(completed_move)
        except:
            Logger('error executing compression console command. \n Command: {}'.format(self.command))

    def __del__(self):
        print('worker Destroyed')
