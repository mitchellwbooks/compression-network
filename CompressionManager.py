from multiprocessing import Pool
import multiprocessing
from time import sleep
import ffmpeg
import json
import os
import logging
import subprocess
import fcntl
from threading import Lock
import shutil


class CompressionManager:
	"""
	Compression Manager

	Daemon that controls the execution of compression search and work jobs.
	"""

	def __init__(self, config_file):
		with open(config_file, 'r') as file:
			self.config = json.load(file)
		self.queue = multiprocessing.Queue()
		self.logger = logging.Logger(logging.WARNING)
		self.manifest = {
			"pending": {},
			"in progress": {},
			"error": {},
			"done": {}
		}
		self.manifest_lock = Lock()
		self.manifest_file = open(self.config['manifest_path'], 'w')

		self.create_manifest()

	def run(self):

		open_jobs = []
		max_open_jobs = self.config['max_open_jobs']
		threads_per_job = self.config['threads_per_job']
		with Pool(max_open_jobs) as pool:
			while True:
				for job_future in open_jobs:
					if job_future.ready():
						del job_future
				if len(open_jobs) == max_open_jobs:
					sleep(30)
					continue
				job = self.get_job_from_manifest()
				if job is None:
					sleep(30)
					# Refresh our job manifest
					self.create_manifest()
					continue
				job_file_path = job[0]
				job_details = job[1]
				job_future = pool.apply_async(self.work_job, args=(job_file_path, job_details, threads_per_job),
											  callback = lambda result: self.mark_job_done(job_file_path, result),
											  error_callback = lambda exception: self.mark_job_error(job_file_path, str(exception)))
				open_jobs.append(job_future)

	@staticmethod
	def work_job(file_path, job_details, threads_per_job):
		input_file = file_path
		output_file = job_details['cache_path']
		stream = ffmpeg.input(input_file)
		compression_kwargs = {
			'map': 0,
			'c': 'copy',
			'c:v': 'libx265',
			'preset': 'slow',
			'crf': 22,
			'threads': threads_per_job,
			'max_muxing_queue_size': 1024,
		}
		stream = ffmpeg.output(stream, output_file, **compression_kwargs )
		output, error = ffmpeg.run(stream, capture_stderr=True)
		return str(error)

	def create_manifest(self):
		for directory in self.config['input_directories']:
			directory_data = self.config['input_directories'][directory]
			for filename in os.listdir(directory):
				file_path = f'{directory}/{filename}'
				if os.path.isfile(file_path):
					self.add_manifest_item(filename, directory, directory_data)

	def add_manifest_item(self, filename, input_directory: str, directory_data: dict):
		file_path = f'{input_directory}/{filename}'
		if '.mkv' not in file_path and '.mp4' not in file_path and '.MOV' not in file_path:
			return
		new_filename = filename.replace('.mp4', '.mkv').replace('.MOV', '.mkv')

		try:
			file_properties = json.loads(
				subprocess.check_output(f"ffprobe -i {file_path} -v quiet -print_format json -show_format",
										shell = True))
		except:
			self.logger.error(
				f'Failed to get properties with ffprobe for compressed file: {file_path}.\ncreate_manifest()')
			return

		self.lock_manifest()
		if file_path in self.manifest['pending'] or file_path in self.manifest['in progress'] or file_path in self.manifest['error']:
			self.unlock_manifest()
			return

		self.manifest['pending'][file_path] = {
			"file_properties": file_properties,
			"cache_path": f'{directory_data["cache_directory"]}/{new_filename}',
			"output_path": f'{directory_data["output_directory"]}/{new_filename}',
			"completed_path": f'{directory_data["completed_directory"]}/{filename}'
		}
		self.unlock_manifest()

	def get_job_from_manifest(self):
		job = None
		self.lock_manifest()
		pending_items = self.manifest['pending'].items()
		if len(pending_items) > 0:
			job = list(pending_items)[0]
			del self.manifest['pending'][job[0]]
			self.manifest['in progress'][job[0]] = job[1]
		self.unlock_manifest()
		return job

	def mark_job_done(self, file_path, output):
		self.lock_manifest()
		job_data = self.manifest['in progress'][file_path]
		del self.manifest['in progress'][file_path]
		job_data['output'] = output
		dest = shutil.move(job_data['cache_path'], job_data['output_path'])
		dest = shutil.move(file_path, job_data['completed_path'])
		self.manifest['done'][file_path] = job_data

		self.unlock_manifest()

	def mark_job_error(self, file_path, exception):
		self.lock_manifest()
		job_data = self.manifest['in progress'][file_path]
		del self.manifest['in progress'][file_path]
		job_data['exception'] = exception
		self.manifest['error'][file_path] = job_data
		self.unlock_manifest()

	def lock_manifest(self):
		self.manifest_lock.acquire()
		fcntl.flock(self.manifest_file, fcntl.LOCK_EX)

	def unlock_manifest(self):
		with open(self.config['manifest_path'], 'w') as manifest_file:
			self.manifest_file.write('')
			json.dump(self.manifest, manifest_file)
		fcntl.flock(self.manifest_file, fcntl.LOCK_UN)
		self.manifest_lock.release()

	def __del__(self):
		fcntl.flock(self.manifest_file, fcntl.LOCK_UN)
