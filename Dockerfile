FROM ubuntu:18.04

RUN apt update
RUN apt install -y software-properties-common
RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt update
RUN apt install -y ffmpeg
RUN apt install -y python3.7 python3-pip

#USER root

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
RUN mkdir /temp
RUN mkdir /HighBitRate
RUN mkdir /scratch
WORKDIR /code
#ADD . /code/
#RUN pip3 install -r requirements.txt