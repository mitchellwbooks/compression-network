
import os, shlex, subprocess
dir_path = os.path.dirname(os.path.realpath(__file__))


def main():
	for filename in os.listdir():
		if os.path.isdir( filename ):
			continue
		if '.mkv' not in filename and '.mp4' not in filename:
			continue

		command = 'ffmpeg -i ' + filename + ' -map 0 -c copy -c:v libx265 -preset slow -crf 22 -max_muxing_queue_size 1024 H265/' + filename

		command = shlex.split( command )
		p = subprocess.Popen( command )
		p.wait()


if __name__ == '__main__':
	main()
